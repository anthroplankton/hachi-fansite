export default class SongSet {
  StreamId = '';

  SongTitle = '';

  SongUrl = '';

  SongArtist = '';

  Version: 'Full' | 'Short' = 'Full';

  StartTime? = 0;

  EndTime? = 0;
}
